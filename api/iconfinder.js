import fetch from 'node-fetch';


const allowCors = fn => async (req, res) => {
  res.setHeader('Access-Control-Allow-Credentials', true)
  res.setHeader('Access-Control-Allow-Origin', '*')
  // another common pattern
  // res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
  res.setHeader('Access-Control-Allow-Methods', 'GET,OPTIONS,PATCH,DELETE,POST,PUT')
  res.setHeader(
    'Access-Control-Allow-Headers',
    'X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version'
  )
  if (req.method === 'OPTIONS') {
    res.status(200).end()
    return
  }
  return await fn(req, res)
}

const handler = (request, response) => {
  console.clear()
  console.log('-----')
  let { q } = request.query;

  
  const options = {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      Authorization: 'Bearer S5Lt6WqmFJ4xsZB4RZjMyeDkGz4y6N8MKx9gZF4Af1e8AiLKAmv304GBN10vV4Cw'
    }
  };
  
  
  fetch(`https://api.iconfinder.com/v4/icons/search?query=${q}&count=200`, options)
  .then(res1 => res1.json())
  .then(res2 => {
    console.log(res2)
    
    return response.status(200).json({ res2 });
  })
  .catch(err => console.error(err));  
}

export default allowCors(handler)