import { off } from "@svgdotjs/svg.js";

const colors = {
  action: "#0077cc",
  ok: "#4db274",
  notok: "#f83538",
  warning: "#FF9100",
  caution: "#FFE100",
  disabled: "#9540BF",
  background: "#242A33",
  default: "#6C8CAC",
  highlight: "#3F4A5A",
  dynamic: "#c7e3ff"
}


export default function handler(request, response) {
  console.clear()
  console.log('-----')
  let { q, w, h } = request.query;
  
  // Fix any malformed format
  q = q.replaceAll(',|', ',0')
  
  //Find all F characters to get reinstate rows
  const startingIndices = [];
  let indexOccurrence = q.indexOf('F', 0);
  
  while(indexOccurrence >= 0) {
    startingIndices.push(indexOccurrence);
    indexOccurrence = q.indexOf('F', indexOccurrence + 1);
  }
  
  let rows = []
  startingIndices.forEach((f, i) => rows.push(q.substring(f, startingIndices[i+1])))

  //build proper array with proper objects
  let MAX_BOOKED = 0
  let MAX_ESTIMATE = 0
  let BookedPath = "M"
  let EstimatePath = "M"
  const Increment = 100 / (rows.length - 1)
  const PROJECTS = {}
  
  
  rows.forEach((row, i) => {
    const splitRow = row.split(',')
    rows[i] = {
      feature: splitRow[0].substring(1, splitRow[0].length),
      project: splitRow[1],
      estimate: Number(splitRow[2]),
      booked: Number(splitRow[3])
    }

    
    if(Number(splitRow[2]) > Number(MAX_ESTIMATE))
    MAX_ESTIMATE = splitRow[2]
    
    if(Number(splitRow[3]) > Number(MAX_BOOKED))
    MAX_BOOKED = splitRow[3]
    
  })
  
  rows = rows.sort((a,b) => a.project < b.project ? 0 : 1)
  
  const CANVAS_HEIGHT = Math.max(MAX_BOOKED, MAX_ESTIMATE)
  const yAxisCount = (Math.ceil(CANVAS_HEIGHT/100)*100) / 10
  
  
  
  rows.forEach((row, i) => {
    
    PROJECTS[row.project] = PROJECTS[row.project] ? PROJECTS[row.project] + 1 : 1
    
    BookedPath += `${i * Increment},${CANVAS_HEIGHT - rows[i].booked}`
    BookedPath += i === rows.length - 1 ? "" : "L"
    
    EstimatePath += `${i * Increment}, ${CANVAS_HEIGHT - rows[i].estimate}`
    EstimatePath += i === rows.length - 1 ? "" : "L"
  })
  
  
  console.log(PROJECTS)

  let projectBackdropOffset = 0


  const svg = `<?xml version="1.0" encoding="utf-8"?>
<svg 
  xmlns="http://www.w3.org/2000/svg" 
  xmlns:xlink="http://www.w3.org/1999/xlink" 
  style="overflow: visible;" 
  width="${w}"
  height="${h}"
  viewBox="-5 -${CANVAS_HEIGHT / 20} 110 ${CANVAS_HEIGHT + (CANVAS_HEIGHT / 5)} "
  preserveAspectRatio="none"
  >
  
  ${[...Array(10)].map((nil, i) => {
    return `<g>
      <path d="M0,${i*yAxisCount}H100" stroke="${colors.dynamic}" stroke-opacity="0.5"/>
      <text 
        text-anchor="start"
        transform="scale(${(Increment * rows.length) / w}, 1) " 
        dx="-20"
        y="${CANVAS_HEIGHT - (i+0.5)*yAxisCount}" 
        fill="${colors.background}" 
        font-size="12px"
        font-family="sans-serif"
      >
        ${(i+1)*yAxisCount}
      </text>
    </g>`
  })}

  ${Object.keys(PROJECTS).map((p, i)=>{
    const Rect = `<rect width="${Increment * PROJECTS[p]}" x="${projectBackdropOffset * Increment}" y="0" height="${CANVAS_HEIGHT}" fill="hsl(${i * 45 + 60},80%,70%)" fill-opacity="0.15" transform="translate(-${Increment / 2}, 0)" ><title>Hello, World!</title></rect>`
    const Text = `<text 
    text-anchor="middle"
    transform="translate(${projectBackdropOffset * Increment}, 0) scale(${(Increment * rows.length) / w}, 1) " 
    dx="20"
    y="${i % 2 ? 40 : 20}" 
    fill="${colors.background}" 
    font-size="12px"
    font-family="sans-serif"
    >
    ${p}
    </text> `
    
    projectBackdropOffset += PROJECTS[p]
    
    return `<g>
      ${Rect}
      ${Text}
    </g>`
  })}

  <path vector-effect="non-scaling-stroke" d="${EstimatePath}" fill="none" stroke="${colors.background}" stroke-opacity="0.3" stroke-width="2px"/>
  <path vector-effect="non-scaling-stroke" d="${BookedPath}" fill="none" stroke="${colors.background}" stroke-opacity="0.15" stroke-width="2px"/>
  
  ${ rows.map( (r,i) => {
      const centerY = CANVAS_HEIGHT - Math.max(r.estimate, r.booked) + (Math.abs((r.booked - r.estimate)) / 2)
      return `
      <g stroke-linecap="round">

        <path 
          vector-effect="non-scaling-stroke"
          d="M${i * Increment},${CANVAS_HEIGHT - r.estimate}V${CANVAS_HEIGHT - r.booked}" 
          stroke="${r.booked > r.estimate ? colors.notok : colors.ok}" 
          stroke-width="2px" 
          title="${r.feature}"
        />
        <rect rx=".3" ry="3" width="4" height="16" fill="#fff" x="${i * Increment - 2}" y="${centerY - 10}" />
        <text 
          transform="translate(${i * Increment},0), scale(${(Increment * rows.length) / w}, 1)" 
          text-anchor="middle"
          x="0" 
          y="${centerY}" 
          fill="${colors.background}" 
          font-size="8px"
          font-family="sans-serif"
          >
          #${r.feature}
          </text> 
      </g>
      `
    })
  }
  
  
  </svg>
  `
  
  response.setHeader("Content-Type","image/svg+xml"); 
  response.writeHead(200);
  response.end(svg);
  
}
//     <path d="m0,${i * Increment}h${r.booked}" stroke="${r.booked > r.estimate ? colors.notok : colors.ok}" stroke-width="10px" />